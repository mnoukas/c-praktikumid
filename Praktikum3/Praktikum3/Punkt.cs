﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Praktikum3
{
    class Punkt
    {
        private int _x1;
        private int _y1;
        private int _x2;
        private int _y2;

        public Punkt(int x1, int y1, int x2, int y2)
        {
            _x1 = x1;
            _y1 = y1;
            _x2 = x2;
            _y2 = y2;
        }

        public double kaugusNullist()
        {
            double vastus = (Math.Sqrt(_x1*_x2 + _y1*_y2));
            return vastus;
        }

//        public double teataAndmed()
//        {
//            double answer = Math.Sqrt()
//            return 0;
//        }
        public double kaugusTeisestPunktist()
        {
            double answer = (Math.Sqrt(_x1*_x2 + _y1*_y2));
            return answer;
        }

        public bool kasOnAlgusPunkt()
        {
            if ((_x1 == 0 && _x2 == 0) && (_y1 == 0 && _y2 == 0))
            {
                return true;
            }
        else
            {
                return false;
            }
        }
    }
}

