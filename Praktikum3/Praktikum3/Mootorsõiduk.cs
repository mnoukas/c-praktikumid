﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Eventing.Reader;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Praktikum3
{
    public class Mootorsõiduk
    {
        private int _kiirus = 0;
        private int _MaxKiirus = 20;
        private bool _uksedAvatud = false;
        private int kiirus;
        private int maxKiirus;

        public Mootorsõiduk(int kiirus, int MaxKiirus, bool uksedAvatud)
        {
            this._kiirus = kiirus;
            this._MaxKiirus = MaxKiirus;
            this._uksedAvatud = uksedAvatud;
        }
    
        public static void Kiirenda(Mootorsõiduk m1)
        {
            m1._kiirus += 10;
        }

        public int KiirendaVeel(int kiirus, Mootorsõiduk m1)
        {
            if (m1._MaxKiirus <= 20)
            {
                int KiirendaUuem = m1._kiirus + kiirus;
                return KiirendaUuem;
            }
            else if (m1._MaxKiirus == 20)
            {
                int araKiirenda = m1._kiirus + 0;
                return araKiirenda;
            }
            return m1._kiirus;
        }

        

        public void Stop(Mootorsõiduk s1)
        {
            s1._kiirus = 0;
        }

        public string Hetkeseis()
        {
            if (_kiirus > 0)
            {
                return "Teie mootorsõiduki hetkekiiruseks on: " + _kiirus;
            }
            else if (_kiirus == 0)
            {
                return "Teie mootorsõiduk seisab";
            }
            return "Midagi läks pekki";
        }



    }
}

