﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Eventing.Reader;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Praktikum3
{
    class Ristkylik
    {
        private int _pikkus;
        private int _laius;

        public Ristkylik(int pikkus, int laius)
        {
            _pikkus = pikkus;
            _laius = laius;
        }
        public int arvutaYmbermoot()
        {
            int Ymbermoot = 2*(_pikkus + _laius);
            return Ymbermoot;
        }

        public bool kasOnVordsed(Ristkylik r1, Ristkylik r2)
        {
            bool kasOn;
            if (r1 == r2)
            {
                kasOn = true;
                return kasOn;
            }
            else
            {
                return false;
            }
        }

        public bool kasOnRuut()
        {
            bool ruut;
            Ristkylik kujund1 = new Ristkylik(10, 30);
            if (kujund1._laius == kujund1._pikkus)
            {
                ruut = true;
                return ruut;
            }
            else
            {
                return false;
            }
        }
    }
}
