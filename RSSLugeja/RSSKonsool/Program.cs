﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

// NB! See on selle jaoks, et saaksime XMList andmeid lugeda
// kasutades LinqToXMLi
using System.Xml.Linq;

namespace RSSKonsool
{
    class Program
    {
        static void Main(string[] args)
        {
            XDocument xdoc = XDocument.Load("http://www.postimees.ee/rss/");

            // Descendants(arvatavasti kõige olulisem meetod),
            // Võimaldab xml-i elemendid üles leida elemendi nime järgi.
            IEnumerable<XElement> query = from x in xdoc.Descendants("item")
                select x;

            foreach (var item in query)
            {
                //item - XElement ehk siis XML Element
                //item.value - elemendi väärtuse (ehk selle, mis on kirjutatud
                // xml elemendi sisse.
                XElement xTitle = item.Element("title");
                if (xTitle != null)
                {
                    Console.WriteLine(xTitle.Value);
                }
                XElement xDescription = item.Element("description");
                if (xDescription != null)
                {
                    Console.WriteLine(xDescription.Value);
                }
                Console.WriteLine("----");
            }
        }
    }
}
