﻿using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Praktikum2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Mida te teha soovite?");
            Console.WriteLine("1: Küsi arvu");
            Console.WriteLine("2: Fahrenheit 2 Kelvin");
            Console.WriteLine("3: Celsius 2 Fahrenheit");
            Console.WriteLine("4: Küsi vanust");
            Console.WriteLine("5: Kolmnurga kontroll");
            Console.WriteLine("6: Sõnadearv");
            Console.WriteLine("7: Küsi vanust");

            int caseSwitch;
            caseSwitch = int.Parse(Console.ReadLine());
            switch (caseSwitch)
            {
                case 1:
                    Console.WriteLine("Küsi arvu");
                    kysiArvu();
                    Console.ReadLine();
                    break;
                case 2:
                    Console.WriteLine("Fahrenheit 2 Kelvin");
                    fahrenheitToKelvin();
                    Console.ReadLine();
                    break;
                case 3:
                    Console.WriteLine("Celsius 2 Fahrenheit");
                    celsiusToFahrenheit();
                    Console.ReadLine();
                    break;
                case 4:
                    Console.WriteLine("Küsi vanust");
                    kysiVanust();
                    Console.ReadLine();
                    break;
                case 5:
                    Console.WriteLine("Kolmnurga kontroll");
                    teeKolmnurk();
                    Console.ReadLine();
                    break;
                case 6:
                    Console.WriteLine("Sõnadearv");
                    getLimitedWords();
                    Console.ReadLine();
                    break;
                case 7:
                    Console.WriteLine("Tagurpidi");
                    tagurpidi();
                    Console.ReadLine();
                    break;
                default:
                    Console.WriteLine("Vale number");
                    break;
            }
        }

        public static int kysiArvu()
        {
            Console.WriteLine("Kirjuta mingi arv.");
            string tulemus = Console.ReadLine();
            int arv;
            if (int.TryParse(tulemus, out arv))
            {
                Console.WriteLine("JESSS");
                return arv;
            }
            else
            {
                return kysiArvu();
            }
        }

        public static void fahrenheitToKelvin()
        {
            double temp;
            Console.WriteLine("Sisesta temperatuur fahrenheitides: ");
            try
            {
                temp = double.Parse(Console.ReadLine());
                Console.WriteLine("Temperatuur converted into Kelvin: " + (((temp - 32) * (5.0 / 9))));
            }
            catch (FormatException ex)
            {
                Console.WriteLine("Juhtus selline asi " + ex.Message);
            }
        }

        public static double celsiusToFahrenheit()
        {
            Console.WriteLine("Kirjuta temperatuur");
            double a = Double.Parse((Console.ReadLine()));
            double Temp = ((9.0/5.0)*a) + 32;
            Console.WriteLine("Uueks temperatuuriks on: " + Temp);
            return Temp;
        }

        public static void kysiVanust()
        {
            int vanus;
            Console.WriteLine("Sisesta number");

            vanus = Convert.ToInt32(Console.ReadLine());
           
            if (vanus >= 18)
            {
                Console.WriteLine("Olete täisealine!");
            }
            else
            {
                Console.WriteLine("Olete alaealine!");
            }
        }

        public static bool teeKolmnurk()
        {
            int a;
            int b;
            int c;

            a = Int32.Parse(Console.ReadLine());
            b = Int32.Parse(Console.ReadLine());
            c = Int32.Parse(Console.ReadLine());
            if ((a + b > c) && (a + c > b) && (b + c > a))
            {
                Console.WriteLine("JESH, ON KOLMNURK");
                return true;
            }
            else
            {
                Console.WriteLine("NOPE, EI OLE KOLMNURK");
                return false;
            }

        }

        public static int getLimitedWords()
        {
            String s;
            s = Console.ReadLine();
            int c = 0;
            for (int i = 1; i < s.Length; i++)
            {
                if (char.IsWhiteSpace(s[i - 1]) == true)
                {
                    if (char.IsLetterOrDigit(s[i]) == true ||
                        char.IsPunctuation(s[i]))
                    {
                        c++;
                    }
                }
            }
            if (s.Length > 2)
            {
                c++;
            }
            return c;
        }
        public static string tagurpidi()
        {
            string s6ne;
            s6ne = Console.ReadLine();
            char[] charArray = s6ne.ToCharArray();
            Array.Reverse(charArray);
            return new String(charArray);
        }
    }
}
